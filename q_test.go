package continuum

import (
	"bytes"
	"testing"
)

func TestQ(t *testing.T) {
	// Create a new continuum.Q with a 1 message buffer
	in := make(chan []byte)
	out := make(chan []byte, 1)
	q := NewQ(in, out)
	q.Run()

	// Send in a message and receive it
	msgIn := []byte("hello")
	in <- msgIn
	msgOut := <-out

	if !bytes.Equal(msgOut, msgIn) {
		t.Errorf("msg should be '%v', is '%v'", msgIn, msgOut)
	}

	// Send two messages in then receive one.
	// Since we have a buffer size of 1 on the out channel,
	// the first message should be discarded.
	msgIn2 := []byte("world")
	in <- msgIn
	in <- msgIn2
	msgOut = <-out

	if !bytes.Equal(msgOut, msgIn2) {
		t.Errorf("msg should be '%v', is '%v'", msgIn, msgOut)
	}
}

func benchthroughput(b *testing.B, sz int) {
	payload := make([]byte, sz)
	in := make(chan []byte)
	out := make(chan []byte, 1)
	q := NewQ(in, out)
	q.Run()

	for i := 0; i < b.N; i++ {
		in <- payload
		msg := <-out
		if len(msg) != sz {
			panic("payload size is wrong")
		}
	}
}

func BenchmarkQThroughput1k(b *testing.B) {
	benchthroughput(b, 1024)
}

func BenchmarkQThroughput16k(b *testing.B) {
	benchthroughput(b, 16384)
}

func BenchmarkQThroughput64k(b *testing.B) {
	benchthroughput(b, 65536)
}
