package continuum

import (
	"sync"
)

// Q is a channel based ringbuffer
type Q struct {
	in   chan []byte
	out  chan []byte
	lock *sync.RWMutex
}

// NewQ returns a new Q
func NewQ(in chan []byte, out chan []byte) *Q {
	return &Q{in: in, out: out, lock: &sync.RWMutex{}}
}

// Run starts the Q
func (q *Q) Run() {
	go func() {
		for msg := range q.in {
			select {
			case q.out <- msg:
			default:
				<-q.out
				q.out <- msg
			}
		}
	}()
}
