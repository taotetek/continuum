# continuum
--
    import "github.com/taotetek/continuum"


## Usage

#### type Q

```go
type Q struct {
}
```

Q is a channel based ringbuffer

#### func  NewQ

```go
func NewQ(in chan []byte, out chan []byte) *Q
```
NewQ returns a new Q

#### func (*Q) Run

```go
func (q *Q) Run() error
```
Run starts the Q
